# Contributor: Aiden Grossman <agrossman154@yahoo.com>
# Maintainer: Aiden Grossman <agrossman154@yahoo.com>
pkgname=jupyter-nbclient
pkgver=0.7.4
pkgrel=0
pkgdesc="Client library for executing notebooks"
url="https://github.com/jupyter/nbclient"
arch="noarch"
license="BSD-3-Clause"
depends="
	py3-jupyter_client
	jupyter-nbformat
	py3-nest_asyncio
	py3-traitlets
	"
makedepends="py3-gpep517 py3-hatchling"
checkdepends="py3-pytest py3-xmltodict jupyter-nbconvert py3-ipykernel py3-flaky"
options="!check" # circular dependency with jupyter-nbconvert
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/jupyter/nbclient/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/nbclient-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest \
		--deselect nbclient/tests/test_client.py::TestExecute::test_widgets \
		--deselect nbclient/tests/test_client.py::test_run_all_notebooks
}

package() {
	python3 -m installer --destdir="$pkgdir" dist/*.whl
}

sha512sums="
24cdb1d8b9ffc4899f15490f15d5a41a3e62c52c2b57d8ee768dc93fda08bb6f1a432306a551ece3d031061f790e032c765b0d96225700b2820ca59bc71e9aeb  jupyter-nbclient-0.7.4.tar.gz
"
