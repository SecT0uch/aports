# Contributor: Alex McGrath <amk@amk.ie>
# Maintainer: Alex McGrath <amk@amk.ie>
pkgname=newsflash
pkgver=2.2.4_git20230427
pkgrel=0
_gitrev=82811b6f012b8b6757acc6351b07009e91eaac2e
pkgdesc="An rss feed reader that supports various web based services"
url="https://gitlab.com/news-flash/news_flash_gtk/"
arch="all !s390x !riscv64 !ppc64le" # cargo, rust, libhandy not found, ring crate fails to build on ppc64le
license="GPL-3.0-only"
makedepends="
	bash
	cargo
	gettext-dev
	libadwaita-dev
	meson
	openssl-dev>3
	sqlite-dev
	webkit2gtk-6.0-dev
	"
subpackages="$pkgname-lang"
source="https://gitlab.com/news-flash/news_flash_gtk/-/archive/$_gitrev/news_flash_gtk-$_gitrev.tar.gz"
options="net !check" # no tests
builddir="$srcdir/news_flash_gtk-$_gitrev"

export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

build() {
	abuild-meson . output
	meson compile -j 1 -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
	ln -s com.gitlab.newsflash "$pkgdir"/usr/bin/newsflash
}

sha512sums="
f0503e69a374ee9bef54dd90d962866ff4bed3ab5aefeb525a853f91f27e15b53758be5bcdc4b9c0d881d62aabe31900b2ab25a6d4a7d44e7637d5ea257dcec6  news_flash_gtk-82811b6f012b8b6757acc6351b07009e91eaac2e.tar.gz
"
