# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=java-postgresql-jdbc
_pkgname=postgresql-jdbc
pkgver=42.5.4
pkgrel=1
pkgdesc="Java JDBC 4.2 (JRE 8+) driver for PostgreSQL database"
url="https://jdbc.postgresql.org/"
# riscv64 blocked by java-jdk
# aarch64: jdk segfaults building it
arch="noarch !aarch64 !riscv64"
license="BSD-3-Clause"
makedepends="java-jdk gradle"
source="$pkgname-$pkgver.tar.gz::https://github.com/pgjdbc/pgjdbc/archive/REL$pkgver.tar.gz
	gradle-8.patch
	"
options="!check net" # tests require running postgres server
builddir="$srcdir/pgjdbc-REL$pkgver"

# secfixes:
#   42.5.1-r0:
#     - CVE-2022-41946
#   42.4.2-r0:
#     - CVE-2022-31197
#   42.2.25-r0:
#     - CVE-2022-21724
#     - CVE-2020-13692

build() {
	# Note: Gradle downloads quite many dependencies, but
	# these are used only for building, not bundled to the final JAR.
	export JAVA_HOME="/usr/lib/jvm/default-jvm"
	gradle --no-daemon -p pgjdbc jar
}

check() {
	gradle --no-daemon -p pgjdbc test
}

package() {
	install -Dm644 ./pgjdbc/build/libs/postgresql-$pkgver-SNAPSHOT.jar \
		"$pkgdir"/usr/share/java/$_pkgname-$pkgver.jar
	ln -s $_pkgname-$pkgver.jar "$pkgdir"/usr/share/java/$_pkgname.jar
}

sha512sums="
22b6164b10239880bc504834d4c495dbc2f92cf2a963d75e145880eaaa708334e5229d4cef0f567896e7838f8586c5564fb5075457cde57904485da519cc19fb  java-postgresql-jdbc-42.5.4.tar.gz
9a4e4389a76956785ed578b87c02fff23c8a20fac18f6d97f0871c64e53cf8bf457970d8ebe8b3b12c9aef602d6dc652421654cf6c51ac5fa4a499e61a3d7807  gradle-8.patch
"
