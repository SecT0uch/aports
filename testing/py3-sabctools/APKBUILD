# Contributor: Fabricio Silva <hi@fabricio.dev>
# Maintainer: Fabricio Silva <hi@fabricio.dev>
pkgname=py3-sabctools
_pkgname=sabctools
pkgver=7.0.1
pkgrel=0
pkgdesc="C implementations of functions for use within SABnzbd"
url="https://github.com/sabnzbd/sabctools"
arch="all"
license="GPL-2.0-or-later AND Apache-2.0 AND CC0-1.0"
makedepends="
	linux-headers
	py3-gpep517
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="
	py3-chardet
	py3-jaraco.functools
	py3-portend
	py3-pytest
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/sabnzbd/sabctools/archive/v$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
aab6667f1414451fbc46b4e1416e0f1e368b1c03cdf7d8850e3a932c89848cefef9e8b6ded59e8781e48d0b786cbd68c7b1229a2f7d4657adf033c584c7260f7  py3-sabctools-7.0.1.tar.gz
"
