# Contributor: Jonah Brüchert <jbb@kaidan.im>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=angelfish
pkgver=23.04.0
pkgrel=0
pkgdesc="Small Webbrowser for Plasma Mobile"
# armhf blocked by extra-cmake-modules
# ppc64le and s390x blocked by qt5-qtwebengine
# riscv64 disabled due to missing rust in recursive dependency
arch="all !ppc64le !s390x !armhf !riscv64"
url="https://phabricator.kde.org/source/plasma-angelfish/"
license="GPL-3.0-or-later"
depends="
	kirigami2
	plasma-framework
	purpose
	qt5-qtbase-sqlite
	qt5-qtfeedback
	qt5-qtquickcontrols2
	"
makedepends="
	corrosion
	extra-cmake-modules
	kdeclarative-dev
	ki18n-dev
	kio-dev
	kirigami-addons-dev
	kirigami2-dev
	plasma-framework-dev
	purpose-dev
	qqc2-desktop-style-dev
	qt5-qtfeedback-dev
	qt5-qtwebengine-dev
	samurai
	"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/angelfish-$pkgver.tar.xz"
options="net" # net required to download Rust dependencies

provides="plasma-angelfish=$pkgver-r$pkgrel" # Backwards compatibility
replaces="plasma-angelfish" # Backwards compatibility

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -j1
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5fd3eee9dd181da645a3583647d98bb586d9d8acaa36a1c19727bc29a97161d2d55c66b2965d1d5b1b935c92ecf379d999da795a8135e6e0129b49cf2bf40e65  angelfish-23.04.0.tar.xz
"
