# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-wordcloud
_pkgorig=wordcloud
pkgver=1.9.1.1
pkgrel=0
pkgdesc="A little word cloud generator in Python"
url="https://github.com/amueller/word_cloud"
arch="all"
license="MIT"
depends="python3 py3-matplotlib py3-numpy py3-pillow"
makedepends="cython python3-dev py3-setuptools"
checkdepends="py3-coverage py3-mock py3-pytest py3-pytest-cov"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver-2.tar.gz::https://github.com/amueller/word_cloud/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/word_cloud-$pkgver"

build() {
	rm -fv wordcloud/query_integral_image.c
	cythonize wordcloud/query_integral_image.pyx
	python3 setup.py build
}

check() {
	PYTHONPATH="$(echo build/lib.linux*)" pytest -vv -k 'not test_unicode_with_stopwords and not test_wordcloud_cli'
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
}

sha512sums="
fad4d7a191dcb10d0531f8a1df9ea78eef01b6bcf1d97f62a4777f03304f4e1b3ad27fffb1d576201c694e1cf1d20737c5b1e03cdfc685d11a9e3d29f3633325  py3-wordcloud-1.9.1.1-2.tar.gz
"
