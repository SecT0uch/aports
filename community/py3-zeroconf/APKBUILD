# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-zeroconf
pkgver=0.58.2
pkgrel=0
pkgdesc="Python implementation of multicast DNS service discovery"
url="https://github.com/jstasiak/python-zeroconf"
arch="all"
license="LGPL-2.0-or-later"
replaces="py-zeroconf" # for backwards compatibility
provides="py-zeroconf=$pkgver-r$pkgrel" # for backwards compatibility
depends="python3 py3-ifaddr"
makedepends="
	cython
	py3-gpep517
	py3-poetry-core
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="py3-pytest py3-pytest-cov py3-pytest-asyncio"
subpackages="$pkgname-pyc"
source="python-zeroconf-$pkgver.tar.gz::https://github.com/jstasiak/python-zeroconf/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir"/python-zeroconf-$pkgver

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
574b2b7055bd622237172c30bdaf85c84d06ac189deb43f6f6f692317cf9d8f15957f0cb126ff33a1f7ca75e5f375e6dd60bc52cf02027bd8f65dc2110f47f3a  python-zeroconf-0.58.2.tar.gz
"
