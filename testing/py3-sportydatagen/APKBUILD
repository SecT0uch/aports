# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-sportydatagen
pkgver=0.2.0
pkgrel=0
pkgdesc="Generator of Endurance Sports Activity Collections (datasets)"
url="https://gitlab.com/firefly-cpp/sportydatagen"
arch="noarch !ppc64le !s390x !riscv64" # py3-sport-activities-features
license="MIT"
depends="python3 py3-niapy py3-numpy py3-pandas py3-sport-activities-features"
makedepends="py3-poetry-core py3-gpep517"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://gitlab.com/firefly-cpp/sportydatagen/-/archive/$pkgver/sportydatagen-$pkgver.tar.gz"
builddir="$srcdir/sportydatagen-$pkgver"
options="!check" # disable tests for now | will be fixed soon

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/sportydatagen-$pkgver-py3-none-any.whl
}

sha512sums="
3d3047ac6bd738ac0923d3d24564af5e051d3378b1526d557762fd7265586eb2e2f16957597b53ff7a7301f732357a9c9fb8d64cd140d77f7b60171e2690e540  py3-sportydatagen-0.2.0.tar.gz
"
