# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-platformdirs
pkgver=3.5.0
pkgrel=0
pkgdesc="Module for determining appropriate platform-specific dirs"
url="https://github.com/platformdirs/platformdirs"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-gpep517 py3-hatchling py3-hatch-vcs"
checkdepends="py3-appdirs py3-pytest py3-pytest-mock"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/p/platformdirs/platformdirs-$pkgver.tar.gz"
builddir="$srcdir/platformdirs-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" dist/*.whl
}

sha512sums="
2c454a760b3045f4824b1c436ac85533e7a6809e4525d70cee4d98289f9efc5a882384347a1992aaa237dc29ea8e778515eadeca208a2d058f9e574f5175ce20  platformdirs-3.5.0.tar.gz
"
